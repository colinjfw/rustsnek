use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::u16;

#[derive(Copy, Clone, Eq, PartialEq)]
struct Node {
  cost: u16,
  pos: (usize, usize),
}

impl Ord for Node {
  fn cmp(&self, other: &Node) -> Ordering {
    other
      .cost
      .cmp(&self.cost)
      .then_with(|| self.pos.cmp(&other.pos))
  }
}

impl PartialOrd for Node {
  fn partial_cmp(&self, other: &Node) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

struct Neighbours {
  width: usize,
  height: usize,
  start: (usize, usize),
  curr: u8,
}

impl Iterator for Neighbours {
  type Item = (usize, usize);

  fn next(&mut self) -> Option<(usize, usize)> {
    let c = self.start.0;
    let r = self.start.1;
    self.curr += 1;

    match self.curr {
      1 => {
        if c == 0 {
          self.next()
        } else {
          Option::Some((c - 1, r))
        }
      }
      2 => {
        if r == 0 {
          self.next()
        } else {
          Option::Some((c, r - 1))
        }
      }
      3 => {
        if c == self.width - 1 {
          self.next()
        } else {
          Option::Some((c + 1, r))
        }
      }
      4 => {
        if r == self.height - 1 {
          self.next()
        } else {
          Option::Some((c, r + 1))
        }
      }
      _ => Option::None,
    }
  }
}

pub const INF: u16 = std::u16::MAX;

pub struct Grid {
  width: usize,
  height: usize,
  cost: Vec<u16>,
}

impl Grid {
  pub fn new(width: usize, height: usize) -> Grid {
    Grid {
      width,
      height,
      cost: vec![INF; width * height],
    }
  }

  fn idx(width: usize, pos: (usize, usize)) -> usize {
    return pos.1 * width + pos.0;
  }

  fn get(&self, pos: (usize, usize)) -> Option<Node> {
    if pos.0 >= self.width || pos.1 >= self.height {
      return Option::None;
    }
    let cost = self.cost[Grid::idx(self.width, pos)];
    Option::Some(Node { pos, cost })
  }

  fn set_cost(&mut self, pos: (usize, usize), cost: u16) {
    if pos.0 >= self.width || pos.1 >= self.height {
      return;
    }
    self.cost[Grid::idx(self.width, pos)] = cost;
  }

  fn get_cost(&self, pos: (usize, usize)) -> u16 {
    if pos.0 >= self.width || pos.1 >= self.height {
      return 0;
    }
    self.cost[Grid::idx(self.width, pos)]
  }

  fn neighbours(&self, pos: (usize, usize)) -> Neighbours {
    Neighbours {
      curr: 0,
      height: self.height,
      width: self.width,
      start: pos,
    }
  }

  pub fn draw<'a>(
    &mut self,
    start: (usize, usize),
    cost_fn: &'a dyn Fn((usize, usize), (usize, usize)) -> u16,
  ) {
    let mut q = BinaryHeap::new();

    self.set_cost(start, 0);
    q.push(Node {
      cost: 0,
      pos: start,
    });

    while let Some(Node { cost, pos }) = q.pop() {
      for adj in self.neighbours(pos) {
        let next = Node {
          cost: Grid::sum(cost, cost_fn(pos, adj)),
          pos: adj,
        };
        if next.cost < self.get_cost(adj) {
          q.push(next);
          self.set_cost(next.pos, next.cost);
        }
      }
    }
  }

  fn sum(a: u16, b: u16) -> u16 {
    match a.checked_add(b) {
      Some(v) => v,
      None => INF,
    }
  }

  pub fn distance_to(&self, pos: (usize, usize)) -> u16 {
    return self.get_cost(pos);
  }

  pub fn pretty_print(&self) {
    const RED: &'static str = "\x1b[0;31m";
    const LIGHT: &'static str = "\x1b[0;34m";
    const BLUE: &'static str = "\x1b[0;36m";
    const NC: &'static str = "\x1b[0m";

    for row in 0..self.height {
      for col in 0..self.width {
        let cost = self.get_cost((row, col));
        if cost == INF {
          print!("|  {}∞{}  ", RED, NC);
        } else if cost == 0 {
          print!("| {}{:3}{} ", BLUE, cost, NC);
        } else {
          print!("| {}{:3}{} ", LIGHT, cost, NC);
        }
      }
      println!("|")
    }
  }
}

mod tests {
  use super::*;

  #[test]
  fn draw_grid() {
    let mut g = Grid::new(5, 5);
    g.draw((0, 0), &|_, _| 1);

    assert_eq!(g.distance_to((0, 0)), 0);
    assert_eq!(g.distance_to((1, 1)), 2);
    assert_eq!(g.distance_to((4, 4)), 8);
  }
}
