use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use uuid::Uuid;

pub enum Direction {
  Up,
  Down,
  Left,
  Right,
}

impl Direction {
  pub fn to_string(self) -> &'static str {
    match self {
      Direction::Up => "up",
      Direction::Down => "down",
      Direction::Left => "left",
      Direction::Right => "right",
    }
  }
}

#[derive(Debug, Deserialize, Serialize, Clone, Copy)]
pub struct Point {
  pub x: usize,
  pub y: usize,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Board {
  pub height: usize,
  pub width: usize,
  pub snakes: Vec<Snake>,
  pub food: Vec<Point>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Snake {
  pub id: Uuid,
  pub body: Vec<Point>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Move {
  pub turn: u32,
  pub board: Board,
  pub you: Snake,
}

#[derive(Debug)]
pub struct State {
  food: HashSet<(usize, usize)>,
  snakes: HashSet<(usize, usize)>,
  state: Move,
}

impl State {
  pub fn new(state: Move) -> State {
    let mut food = HashSet::with_capacity(state.board.food.len());
    let mut snakes = HashSet::with_capacity(state.board.snakes.len());
    for f in &state.board.food {
      food.insert((f.x, f.y));
    }
    for s in &state.board.snakes {
      for p in &s.body {
        snakes.insert((p.x, p.y));
      }
    }
    State {
      state,
      food,
      snakes,
    }
  }

  pub fn my_head(&self) -> (usize, usize) {
    let p = self.state.you.body[0];
    (p.x, p.y)
  }

  pub fn has_snake(&self, pos: (usize, usize)) -> bool {
    self.snakes.contains(&pos)
  }

  pub fn has_food(&self, pos: (usize, usize)) -> bool {
    self.food.contains(&pos)
  }
}
