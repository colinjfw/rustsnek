use crate::graph::*;
use crate::snake::*;

pub fn run(m: Move) -> Direction {
  let mut grid = Grid::new(m.board.width, m.board.height);
  let state = State::new(m);
  let cost = |_, end| {
    if state.has_snake(end) {
      INF
    } else {
      1
    }
  };

  grid.draw(state.my_head(), &cost);
  grid.pretty_print();

  Direction::Up
}

mod tests {
  use super::*;

  fn run_state(name: &'static str) {
    let state = std::fs::read_to_string(name).unwrap();
    let m: Move = serde_json::from_str(state.as_str()).unwrap();
    run(m);
  }

  #[test]
  fn state_basic() {
    run_state("./src/states/basic.json");
  }
}
